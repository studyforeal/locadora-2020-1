package br.ucsal.bes20201.testequalidade.locadora;




import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


import br.ucsal.bes20201.testequalidade.builder.ClienteBuilder;
import br.ucsal.bes20201.testequalidade.builder.VeiculoBuilder;
import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Cliente;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Locacao;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.exception.CampoObrigatorioNaoInformado;
import br.ucsal.bes20201.testequalidade.locadora.exception.ClienteNaoEncontradoException;
import br.ucsal.bes20201.testequalidade.locadora.exception.LocacaoNaoEncontradaException;
import br.ucsal.bes20201.testequalidade.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.bes20201.testequalidade.locadora.persistence.ClienteDAO;
import br.ucsal.bes20201.testequalidade.locadora.persistence.LocacaoDAO;
import br.ucsal.bes20201.testequalidade.locadora.persistence.VeiculoDAO;
import br.ucsal.bes20201.testequalidade.locadora.util.ValidadorUtil;

public class LocacaoBOIntegradoTest {

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 */
	
	public static ClienteBuilder builderCliente;
	public static VeiculoBuilder builderVeiculo;
	
	@BeforeAll
	public static void setup() {
		
		builderCliente = ClienteBuilder.clienteDefault();
		builderVeiculo = VeiculoBuilder.veiculoDefault();
	}
	
	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() throws ClienteNaoEncontradoException, VeiculoNaoEncontradoException, LocacaoNaoEncontradaException {
		Cliente cliente = builderCliente.but().withCPF("234.233.411-40")
											   .withNome("Sicrano da Silva")
											   .withTelefone("71939492392")
											   .build();
		ClienteDAO.insert(cliente);
		cliente = ClienteDAO.obterPorCpf(cliente.getCpf());
		Veiculo veiculo1 = builderVeiculo.but().build();
		Veiculo veiculo2 = builderVeiculo.but().withPlaca("923-3234").withAno(2019).withValorDiaria(100.0).build();
		Veiculo veiculo3 = builderVeiculo.but().withPlaca("OSF-42KW").withAno(2015).withValorDiaria(60.0).build();
		Veiculo veiculo4 = builderVeiculo.but().withPlaca("PWO-0742").withAno(2014).withValorDiaria(50.0).build();
		Veiculo veiculo5 = builderVeiculo.but().withPlaca("POM-SPO2").withAno(2020).withValorDiaria(120.0).build();
		VeiculoDAO.insert(veiculo1);
		VeiculoDAO.insert(veiculo2);
		VeiculoDAO.insert(veiculo3);
		VeiculoDAO.insert(veiculo4);
		VeiculoDAO.insert(veiculo5);
		List<Veiculo> veiculos = new ArrayList<>();
		veiculos.add(VeiculoDAO.obterPorPlaca(veiculo1.getPlaca()));
		veiculos.add(VeiculoDAO.obterPorPlaca(veiculo2.getPlaca()));
		veiculos.add(VeiculoDAO.obterPorPlaca(veiculo3.getPlaca()));
		veiculos.add(VeiculoDAO.obterPorPlaca(veiculo4.getPlaca()));
		veiculos.add(VeiculoDAO.obterPorPlaca(veiculo5.getPlaca()));
		Locacao locacao = new Locacao(cliente,veiculos,new Date(),3);
		LocacaoDAO.insert(locacao);
		locacao = LocacaoDAO.obterPorNumeroContrato(locacao.getNumeroContrato());
	    Double totalEsperado = 1056.0;
	    Double totalAtual = LocacaoBO.calcularValorTotalLocacao(locacao.getVeiculos(), locacao.getQuantidadeDiasLocacao());
		assertEquals(totalEsperado,totalAtual);
	}
	
	@Test
	public void testarExceptions() {
	 assertThrows(ClienteNaoEncontradoException.class, ()-> {
		 ClienteDAO.obterPorCpf("9999");
	 });
	 
	 assertThrows(VeiculoNaoEncontradoException.class, ()-> {
		 VeiculoDAO.obterPorPlaca("293-9999");
	 });
	 
	 assertThrows(LocacaoNaoEncontradaException.class, ()-> {
		 LocacaoDAO.obterPorNumeroContrato(90);
	 });
	 
	 assertThrows(CampoObrigatorioNaoInformado.class, ()-> {
		 Date data = null;
		 ValidadorUtil.validarCampoObrigatorio(data, "data");
	 });
	 
	 assertThrows(CampoObrigatorioNaoInformado.class, ()-> {
		 Integer numero = null;
		 ValidadorUtil.validarCampoObrigatorio(numero, "numero");
	 });
	 
	 assertThrows(CampoObrigatorioNaoInformado.class, ()-> {
		 List lista = null;
		 ValidadorUtil.validarCampoObrigatorio(lista, "lista");
	 });
	 
	 
	 
	}


}
